import mysql from 'mysql2'

var conexion = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "d1n0s4ur10",
    database: "sistemas"
});

conexion.connect(function(err){
    if(err){
        console.log("surgio un error :<   " + err);
    } else{
        console.log("Conexion exitosa :>")
    }
})

export default conexion;