import express from "express";

export const router = express.Router();

export default { router };

// Configurar ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Examen 2" });
});


// Configurar ruta
router.get("/pago", (req, res) => {
  const params = {
      isPost: false,
  };
  res.render("pago", params);
});

router.post("/pago", (req, res) => {
  const numdoc = req.body.numdoc;
  const nombre = req.body.nombre;
  const domicilio = req.body.domicilio;
  const nivel = Number(req.body.nivel);
  const pagohorabase = req.body.pagohorabase;
  const horasimpartidas = req.body.horasimpartidas;
  const canthijos = Number(req.body.canthijos);

  //calculando salario
  let porcentajeincremento;
    
  if (nivel === 1) {
      porcentajeincremento = 0.3;
  } else if (nivel === 2) {
      porcentajeincremento = 0.5;
  } else if (nivel === 3) {
      porcentajeincremento = 1;
  } else {
      porcentajeincremento = 0;
  }

  let pagoBase = pagohorabase * horasimpartidas;
  let pagoTotal = pagoBase * (1 + porcentajeincremento);
  let pagoxhora = pagoTotal;
  let impuesto = pagoTotal * 0.16;


  // Calcular bono
  let bono;

  if (canthijos === 0) {
      bono = 0;
  } else if (canthijos === 1 || canthijos === 2) {
      bono = pagoTotal * 0.05;
  } else if (canthijos > 2 && canthijos < 6) {
      bono = pagoTotal * 0.1;
  } else {
      bono = pagoTotal * 0.2;
  }
  

    pagoTotal = pagoTotal + bono;
     // Calcular impuesto
    pagoTotal = pagoTotal-impuesto;

 const params = {
    numdoc,
    nombre,
    domicilio,
    nivel,
    pagohorabase,
    horasimpartidas,
    canthijos,
    bono,
    impuesto,
    pagoxhora,
    pagoTotal,
    isPost: true, 
 }
res.render("pago", params);
});

